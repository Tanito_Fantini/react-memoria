import React from 'react';
import '../styles/Header.css';
import Reloj from './Reloj.js';

function Header(props) {
  var estado_reloj = '';

  switch( props.estado ) {
    case 'iniciado':
      estado_reloj = 'correr';
      break;

    case 'pausado':
      estado_reloj = 'pausar';
      break;
    
    case 'terminado':
      estado_reloj = 'parar';
      break;

    default:
      estado_reloj = 'parar';
  }

  return (
    <header>
      <div className="titulo">React-Memoria</div>
      <div className="controles">
        <button className="btn btn-iniciar" onClick={props.iniciarPartida} disabled={ props.estado === 'iniciado' }>
          Iniciar
        </button>
        <button className="btn btn-pausar" onClick={props.pausarPartida} disabled={ props.estado !== 'iniciado' }>
          Pausar
        </button>
        <button className="btn btn-reiniciar" onClick={props.reiniciarPartida} disabled={ props.estado !== 'iniciado' }>
          Reiniciar
        </button>
        <button className="btn btn-guardar" onClick={props.guardarPartida} disabled={ props.estado !== 'iniciado' }>
          Guardar
        </button>
      </div>
      <div className="reloj">
        <Reloj
          segundos={ props.segundos }
          minutos={ props.minutos }
          horas={ props.horas }
          estado={ estado_reloj }
          onTick={ props.onTick }
        />
      </div>
      <div className="intentos">
        Intentos: { props.intentos }
      </div>
    </header>
  );
}

export default Header;