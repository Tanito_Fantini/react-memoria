## Juego de Memoria con React.js

Este repositorio está basado en el curso: [Juego de memoria con React.js](http://www.appdelante.com/cursos/juego-de-memoria-react).

#### Requisitos:

El único requisito para ejecutar el projecto es tener instalado [Node.js](https://nodejs.org/en/download/).

Una vez que hayas instalado Node.js sigue los siguientes pasos: 

1. Abre la terminal y ejecuta: `git clone https://Tanito_Fantini@bitbucket.org/Tanito_Fantini/react-memoria.git` para descargar el projecto.
2. Ejecuta el comando: `cd react-memoria` para entrar al directorio del proyecto.
3. Ejecuta el comando: `npm install` para instalar las dependencias.
4. Ejecuta el comando: `npm start` para correr la aplicación.
5. Abre en tu navegador el siguiente URL [http://localhost:3000](http://localhost:3000).