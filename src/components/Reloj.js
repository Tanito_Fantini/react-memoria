import React, { Component } from 'react';
import '../styles/Reloj.css';
import PropTypes from 'prop-types';

class Reloj extends Component {

  constructor(props) {
    super(props);

    this.timerID = null;

    this.state = {
      segundos: 0,
      minutos: 0,
      horas: 0
    };

    this.correr = this.correr.bind(this);
    this.reiniciar = this.reiniciar.bind(this);
    this.pausar = this.pausar.bind(this);
  }

  componentDidMount() {
    if( this.props.iniciar !== undefined && this.props.iniciar ) {
      this.correr();
    }

    if( this.props.segundos && this.props.segundos > 0 && this.props.segundos < 60 ) {
      this.setState({
        segundos: this.props.segundos
      });
    }

    if( this.props.minutos && this.props.minutos > 0 && this.props.minutos < 60 ) {
      this.setState({
        minutos: this.props.minutos
      });
    }

    if( this.props.horas && this.props.horas > 0 ) {
      this.setState({
        horas: this.props.horas
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if( this.props.estado !== undefined && prevProps.estado !== this.props.estado ) {
      switch( this.props.estado ) {
        case 'correr':
          this.correr();
          break;
        
        case 'reiniciar':
          this.reiniciar();
          break;
        
        case 'pausar':
          this.pausar();
          break;

        case 'parar':
          this.parar();
          break;
        
        default:
      }
    }

    if( this.props.segundos !== undefined && prevProps.segundos !== this.props.segundos ) {
      this.setState({
        segundos: this.props.segundos
      });
    }

    if( this.props.minutos !== undefined && prevProps.minutos !== this.props.minutos ) {
      this.setState({
        minutos: this.props.minutos
      });
    }

    if( this.props.horas !== undefined && prevProps.horas !== this.props.horas ) {
      this.setState({
        horas: this.props.horas
      });
    }
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    var
        segundos = this.state.segundos,
        minutos = this.state.minutos,
        horas = this.state.horas;

    segundos += 1;

    if( segundos > 59 ) {
      segundos = 0;
      minutos += 1;
    }

    if( minutos > 59 ) {
      minutos = 0;
      horas += 1;
    }

    if( this.props.onTick ) {
      this.props.onTick({
        segundos,
        minutos,
        horas
      });
    }

    this.setState({
      segundos,
      minutos,
      horas
    });
  }

  render() {
    return (
      <div className="reloj">
        { this.format() }
      </div>
    );
  }

  format() {
    var
        segundos = this.state.segundos,
        minutos = this.state.minutos,
        horas = this.state.horas;
    
    if( segundos < 10 ) {
      segundos = `0${segundos}`;
    }

    if( minutos < 10 ) {
      minutos = `0${minutos}`;
    }

    if( horas < 10 ) {
      horas = `0${horas}`;
    }

    return `${horas}:${minutos}:${segundos}`;
  }

  reiniciar() {
    if( this.state.segundos > 0 || this.state.minutos > 0 || this.state.horas > 0 ) {
      this.setState({
        segundos: 0,
        minutos: 0,
        horas: 0
      });
    }
  }

  pausar() {
    if( this.timerID !== null ) {
      clearInterval(this.timerID);

      this.timerID = null;
    }
  }

  correr() {
    if( this.timerID === null ) {
      this.timerID = setInterval(() => {
        this.tick();
      }, 1000);
    }
  }

  parar() {
    if( this.timerID !== null ) {
      this.pausar();
      this.reiniciar();
    }
  }

}

Reloj.propTypes = {
  iniciar: PropTypes.bool,
  estado: PropTypes.string,
  segundos: PropTypes.number,
  minutos: PropTypes.number,
  horas: PropTypes.number,
  onTick: PropTypes.func
};

export default Reloj;