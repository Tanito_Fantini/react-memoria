import React, { Component } from 'react';
import Header from './Header';
import Tablero from './Tablero';
import '../styles/App.css';
import construirBaraja from '../utils/construirBaraja';

const estado = {
  INICIADO: 'iniciado',
  PAUSADO: 'pausado',
  REINICIAR: 'reiniciar',
  DETENIDO: 'detenido',
  TERMINADO: 'terminado'
};

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      estado: estado.DETENIDO,
      baraja: [],
      parejaSeleccionada: [],
      estaComparando: false,
      intentos: 0,
      cantidad_cartas: 0
    };

    this.iniciar          = this.iniciar.bind(this);
    this.pausar           = this.pausar.bind(this);
    this.reiniciar        = this.reiniciar.bind(this);
    this.guardar          = this.guardar.bind(this);
    this.seleccionarCarta = this.seleccionarCarta.bind(this);
    this.onTick           = this.onTick.bind(this);
  }

  componentDidMount() {
    var data = localStorage.getItem("memoria");

    // parse the localStorage string and setState
    try {
      if( data !== null ) {
        data = JSON.parse(data);

        this.setState({
          estado: estado.INICIADO,
          baraja: data.baraja,
          intentos: data.intentos,
          tiempo_inicial: data.tiempo
        });
      }
    } catch (e) {
    }
  }

  render() {
    var
        segundos = 0,
        minutos  = 0,
        horas    = 0;

    if( this.state.tiempo_inicial !== undefined ) {
      segundos = this.state.tiempo_inicial.segundos;
      minutos  = this.state.tiempo_inicial.minutos;
      horas    = this.state.tiempo_inicial.horas;
    }
    
    return (
      <div className="App">
        <Header
          estado={ this.state.estado }
          segundos={ segundos }
          minutos={ minutos }
          horas={ horas }
          iniciarPartida={ this.iniciar }
          pausarPartida={ this.pausar }
          reiniciarPartida={ this.reiniciar }
          guardarPartida={ this.guardar }
          intentos={ this.state.intentos }
          onTick={ this.onTick }
        />
        <Tablero
          baraja={ this.state.baraja }
          parejaSeleccionada={ this.state.parejaSeleccionada }
          seleccionarCarta={ this.seleccionarCarta }
        />
      </div>
    );
  }

  iniciar() {
    if( this.state.estado !== estado.INICIADO ) {
      let baraja = this.state.baraja;

      if( this.state.baraja.length === 0 || this.state.estado === estado.TERMINADO ) {
        baraja = construirBaraja(20);
      }
  
      this.setState({
        baraja,
        cantidad_cartas: baraja.length,
        estado: estado.INICIADO
      });
    }
  }

  pausar() {
    this.setState({
      estado: estado.PAUSADO
    });
  }

  reiniciar() {
    this.setState({
      estado: estado.REINICIAR
    });

    setTimeout(() => {
      var baraja = construirBaraja(20);

      this.setState({
        estado: estado.INICIADO,
        baraja,
        cantidad_cartas: baraja.length,
        parejaSeleccionada: [],
        estaComparando: false,
        intentos: 0
      });

      localStorage.removeItem('memoria');
    }, 500);
  }

  guardar() {
    var data = {
      baraja: this.state.baraja,
      intentos: this.state.intentos,
      tiempo: this.state.tiempo
    };
    
    localStorage.setItem('memoria', JSON.stringify(data));
  }

  seleccionarCarta(carta) {
    if(
      this.state.estado !== estado.INICIADO ||
      this.state.estaComparando ||
      this.state.parejaSeleccionada.indexOf(carta) > -1 ||
      carta.fue_adivinada
    ) {
      return;
    }

    const parejaSeleccionada = [...this.state.parejaSeleccionada, carta];
    this.setState({
      parejaSeleccionada
    });

    if( parejaSeleccionada.length === 2 ) {
      this.compararPareja(parejaSeleccionada);
    }
  }

  compararPareja(parejaSeleccionada) {
    this.setState({
      estaComparando: true
    });

    setTimeout(() => {
      const [primeraCarta, segundaCarta] = parejaSeleccionada;
      let baraja = this.state.baraja;

      if( primeraCarta.icono === segundaCarta.icono ) {
        baraja = baraja.map((carta) => {
          if( carta.icono !== primeraCarta.icono ) {
            return carta;
          }

          return {...carta, fue_adivinada: true};
        });
      }

      this.verificarSiHayGanador(baraja);

      this.setState({
        parejaSeleccionada: [],
        baraja,
        estaComparando: false
      });
    }, 1000)
  }

  verificarSiHayGanador(baraja) {
    if( baraja.filter((carta) => !carta.fue_adivinada).length === 0 ) {
      let
          segundos = this.state.tiempo.segundos,
          minutos  = this.state.tiempo.minutos,
          horas    = this.state.tiempo.horas;

      if( segundos < 10 ) {
        segundos = `0${segundos}`;
      }
  
      if( minutos < 10 ) {
        minutos = `0${minutos}`;
      }
  
      if( horas < 10 ) {
        horas = `0${horas}`;
      }

      this.setState({
        estado: estado.TERMINADO
      });

      localStorage.removeItem('memoria');

      alert(`Terminaste en ${horas}:${minutos}:${segundos} y con ${this.state.intentos} intentos!`);
    }
    else {
      this.setState({
        intentos: this.state.intentos + 1
      });
    }
  }

  onTick(tiempo) {
    this.setState({
      tiempo
    });
  }

}

export default App;
