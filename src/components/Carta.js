import React from 'react';
import '../styles/Carta.css';
import FlipCard from 'react-flipcard';

function Carta(props) {
  var clase_icono = 'fa-';

  switch( props.clase ) {
    case 'carta-40':
    case 'carta-60':
    case 'carta-80':
      clase_icono += '3x';
      break;

    default:
      clase_icono += '5x';
  }
  
  return (
    <div className={`carta ${props.clase}`} onClick={ props.seleccionarCarta }>
      <FlipCard
        flipped={ props.estaSiendoComparada || props.fue_adivinada }
        disabled={ true }
      >
        <div className={`portada ${props.clase}`}></div>
        <div className={`contenido ${props.clase}`}>
          <i className={`fa ${props.icono} ${clase_icono}`}></i>
        </div>
      </FlipCard>
    </div>
  );
}

export default Carta;