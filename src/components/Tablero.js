import React from 'react';
import Carta from './Carta'
import '../styles/Tablero.css'

function Tablero(props) {
  const cartas = props.baraja.map((carta, index) => {
    const estaSiendoComparada = props.parejaSeleccionada.indexOf(carta) > -1;

    return (
      <Carta
        key={ index }
        icono={ carta.icono }
        estaSiendoComparada={ estaSiendoComparada }
        seleccionarCarta={ () => props.seleccionarCarta(carta) }
        fue_adivinada={ carta.fue_adivinada }
        clase={ `carta-${props.baraja.length}` }
      />
    );
  });
  
  return (
    <div className={`tablero tablero-${props.baraja.length}`}>
      { cartas }
    </div>
  );
}

export default Tablero;