import shuffle from 'lodash.shuffle';
import FontAwesomeClasses from './fontAwesomeClasses';

export default (cantidad_cartas = 20) =>  {
  const fontAwesomeClasses = FontAwesomeClasses();

  let cartas = [];

  if( cantidad_cartas % 2 !== 0 ) {
    cantidad_cartas += 1;
  }

  while( cartas.length < cantidad_cartas ) {
    const index = Math.floor(Math.random() * fontAwesomeClasses.length);
    const carta = {
      icono: fontAwesomeClasses.splice(index, 1)[0],
      fue_adivinada: false
    };

    cartas.push(carta);
    cartas.push({ ...carta });
  }

  return shuffle(cartas);
};
